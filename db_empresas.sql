-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 30/10/2020 às 21:33
-- Versão do servidor: 8.0.22-0ubuntu0.20.04.2
-- Versão do PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `db_empresas`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `enterprise`
--

CREATE TABLE `enterprise` (
  `id` bigint NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `email_enterprise` varchar(255) DEFAULT NULL,
  `enterprise_name` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `own_enterprise` bit(1) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `photo` tinyblob,
  `share_price` float NOT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `value` int NOT NULL,
  `enterprise_type_id` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Despejando dados para a tabela `enterprise`
--

INSERT INTO `enterprise` (`id`, `city`, `country`, `description`, `email_enterprise`, `enterprise_name`, `facebook`, `linkedin`, `own_enterprise`, `phone`, `photo`, `share_price`, `twitter`, `value`, `enterprise_type_id`) VALUES
(1, 'Bristol', 'UK', 'FluoretiQ is a Bristol based medtech start-up developing diagnostic technology to enable bacteria identification within the average consultation window, so that patients can get the right anti-biotics from the start.  ', NULL, 'Fluoretiq Limited', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f312f3234302e6a7065677702002f78, 0, NULL, 0, 13),
(2, 'London', 'UK', 'Service and Product Design, Responsible Business', NULL, 'Little Bee Community', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400232f75706c6f6164732f656e74657270726973652f70686f746f2f322f3234302e706e677702002f78, 0, NULL, 0, 12),
(3, 'Manchester', 'UK', 'Personality personalised \'Done in a Day\' video production ', NULL, 'HSQ Productions', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f332f3234302e6a7065677702002f78, 0, NULL, 0, 12),
(4, 'Alton', 'UK', 'We are reinventing the sales channel through our bespoke platform, myvendorlink, which aims to bring the best niche, new and emerging technology directly to IT resellers - and vice versa!', NULL, 'VendorLink', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f342f3234302e6a7065677702002f78, 0, NULL, 0, 14),
(5, 'London', 'UK', 'TOAD.ai is an online tool for businesses and ad agencies looking for a quick way to plan and buy outdoor advertising campaigns. Rather than contacting multiple media owners or going to slow and expensive media planners, TOAD generates proposals in seconds, saving time and money.\n\nAdvertising signage is a powerful medium that dominates our built environment yet it lacks a consolidated trading platform. By combining the UK’s advertising signage onto a single platform and layering it with audience data, behavioural insights and planning automation, TOAD.ai lets you book campaigns optimized to reach target audiences.\n\nTHINK BIGGER!\n', NULL, 'TOAD.ai', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f352f3234302e6a7065677702002f78, 0, NULL, 0, 16),
(6, 'London', 'UK', 'Problem identified: People have difficulties managing money and coordinating across multiple accounts.\n\nSolution: Bring all accounts in one place for easier management using a goal-based approach and third party service recommendations for efficient spending, everything inside a mobile app.\n\nOur goal is to help people get more value from their money.', NULL, 'Veuno Ltd.', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f362f3234302e6a7065677702002f78, 0, NULL, 0, 17),
(7, 'London', 'UK', 'Realchangers connects mission-driven individuals to careers at impact-driven companies which align with their passion, interests, values & skills.', NULL, 'Realchangers', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f372f3234302e6a7065677702002f78, 0, NULL, 0, 18),
(8, 'Winchester', 'UK', 'Learnsmarter\'s training management solution supports companies and organisations to scale and prioritise efforts to train and develop their employees and their customers.  Fully integrated with Salesforce,  Learnsmarter is the easiest, fastest and most secure way to set up, manage and deliver instructor led training or coaching in the cloud.\n', NULL, 'Learnsmarter Apps Ltd', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f382f3234302e6a7065677702002f78, 0, NULL, 0, 12),
(9, 'London', 'UK', 'We have an equity only crowdfunding platform for property developers and investors.', NULL, 'Invest In Crowd Ltd', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f392f3234302e6a7065677702002f78, 0, NULL, 0, 19),
(10, 'Cardiff', 'UK', 'An automated matching making service for investors from around the world and entrepreneurs looking for finance. The tech is built and the data is collected. The company will launch in the next two months. This is an investment in a company that finds its competition investment!', NULL, 'Investment Searcher', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400252f75706c6f6164732f656e74657270726973652f70686f746f2f31312f3234302e6a7065677702002f78, 0, NULL, 0, 20),
(11, 'London', 'UK', 'Guild is a private professional messaging app that meets new standards, yet is as easy to use as WhatsApp. Advertising free and GDPR compliant.', NULL, 'Guild', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400252f75706c6f6164732f656e74657270726973652f70686f746f2f31322f3234302e6a7065677702002f78, 0, NULL, 0, 11),
(12, 'London', 'UK', 'Creating awesome experiencing by empowering festivals, conferences and sporting events to sell products and services to their customers before, during and after events. Currently working with customers in 13 countries on 5 continents.', NULL, 'Wired Lion', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400252f75706c6f6164732f656e74657270726973652f70686f746f2f31332f3234302e6a7065677702002f78, 0, NULL, 0, 11),
(13, 'Media City ', 'UK', 'Custom video syndication, data and distribution. Through artificial intelligence, we deliver the content your viewers want so they never leave your page.  ', NULL, 'VzzR.io', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f33302f3234302e706e677702002f78, 0, NULL, 0, 11),
(14, 'Media City ', 'UK', 'Custom video syndication, data and distribution. Through artificial intelligence, we deliver the content your viewers want so they never leave your page.  ', NULL, 'VzzR.io', NULL, NULL, b'0', NULL, 0xaced00057372000c6a6176612e696f2e46696c65042da4450e0de4ff0300014c0004706174687400124c6a6176612f6c616e672f537472696e673b78707400242f75706c6f6164732f656e74657270726973652f70686f746f2f33302f3234302e706e677702002f78, 0, NULL, 0, 11);

-- --------------------------------------------------------

--
-- Estrutura para tabela `enterprise_type`
--

CREATE TABLE `enterprise_type` (
  `id` bigint NOT NULL,
  `enterprise_type_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Despejando dados para a tabela `enterprise_type`
--

INSERT INTO `enterprise_type` (`id`, `enterprise_type_name`) VALUES
(11, 'Software'),
(12, 'Service'),
(13, 'Social'),
(14, 'IT'),
(15, 'Marketplace'),
(16, 'Marketplace'),
(17, 'Fintech'),
(18, 'HR Tech'),
(19, 'Fintech'),
(20, 'Fintech');

-- --------------------------------------------------------

--
-- Estrutura para tabela `investor`
--

CREATE TABLE `investor` (
  `id` bigint NOT NULL,
  `balance` float NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_access` bit(1) NOT NULL,
  `investor_name` varchar(255) DEFAULT NULL,
  `photo` tinyblob,
  `portfolio_value` float NOT NULL,
  `super_angel` bit(1) NOT NULL,
  `portfolio_id` bigint DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Despejando dados para a tabela `investor`
--

INSERT INTO `investor` (`id`, `balance`, `city`, `country`, `email`, `first_access`, `investor_name`, `photo`, `portfolio_value`, `super_angel`, `portfolio_id`, `password`) VALUES
(1, 1000000, 'BH', 'Brasil', 'testeapple@ioasys.com.br', b'0', 'Teste Apple', NULL, 1000000, b'0', 1, '$2a$10$KxDzUIEmbMGEARRGfIraguWr/aFeuhxlR/rjKwi0S.YiOCatdbyHi');

-- --------------------------------------------------------

--
-- Estrutura para tabela `investor_perfis`
--

CREATE TABLE `investor_perfis` (
  `investor_id` bigint NOT NULL,
  `perfis_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `perfil`
--

CREATE TABLE `perfil` (
  `id` bigint NOT NULL,
  `nivel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `portfolio`
--

CREATE TABLE `portfolio` (
  `id` bigint NOT NULL,
  `enterprises` tinyblob,
  `enterprises_number` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Despejando dados para a tabela `portfolio`
--

INSERT INTO `portfolio` (`id`, `enterprises`, `enterprises_number`) VALUES
(1, NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `sign_in_response`
--

CREATE TABLE `sign_in_response` (
  `id` bigint NOT NULL,
  `enterprise` varchar(255) DEFAULT NULL,
  `success` bit(1) NOT NULL,
  `investor_id` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Despejando dados para a tabela `sign_in_response`
--

INSERT INTO `sign_in_response` (`id`, `enterprise`, `success`, `investor_id`) VALUES
(1, NULL, b'1', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `sign_in_response_perfis`
--

CREATE TABLE `sign_in_response_perfis` (
  `sign_in_response_id` bigint NOT NULL,
  `perfis_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `enterprise`
--
ALTER TABLE `enterprise`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8p6iv7f5krikp26pi96ej4th1` (`enterprise_type_id`);

--
-- Índices de tabela `enterprise_type`
--
ALTER TABLE `enterprise_type`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `investor`
--
ALTER TABLE `investor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3yxddgavk2o9ddnm65jsepl9e` (`portfolio_id`);

--
-- Índices de tabela `investor_perfis`
--
ALTER TABLE `investor_perfis`
  ADD KEY `FKneda04f72d1k8woriermo3f2d` (`perfis_id`),
  ADD KEY `FKj5apxhtixx9wv1c4eq108722` (`investor_id`);

--
-- Índices de tabela `perfil`
--
ALTER TABLE `perfil`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `sign_in_response`
--
ALTER TABLE `sign_in_response`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK32lt55cv17x08xgtupifhfri1` (`investor_id`);

--
-- Índices de tabela `sign_in_response_perfis`
--
ALTER TABLE `sign_in_response_perfis`
  ADD KEY `FKs2tp5mhxfxnl6iv84nx4ddxsq` (`perfis_id`),
  ADD KEY `FKk6mop1reonrcr2ui8a3mldtlv` (`sign_in_response_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `enterprise`
--
ALTER TABLE `enterprise`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de tabela `enterprise_type`
--
ALTER TABLE `enterprise_type`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de tabela `investor`
--
ALTER TABLE `investor`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `perfil`
--
ALTER TABLE `perfil`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `enterprise`
--
ALTER TABLE `enterprise`
  ADD CONSTRAINT `FK8p6iv7f5krikp26pi96ej4th1` FOREIGN KEY (`enterprise_type_id`) REFERENCES `enterprise_type` (`id`);

--
-- Restrições para tabelas `investor`
--
ALTER TABLE `investor`
  ADD CONSTRAINT `FK3yxddgavk2o9ddnm65jsepl9e` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolio` (`id`);

--
-- Restrições para tabelas `investor_perfis`
--
ALTER TABLE `investor_perfis`
  ADD CONSTRAINT `FKj5apxhtixx9wv1c4eq108722` FOREIGN KEY (`investor_id`) REFERENCES `investor` (`id`),
  ADD CONSTRAINT `FKneda04f72d1k8woriermo3f2d` FOREIGN KEY (`perfis_id`) REFERENCES `perfil` (`id`);

--
-- Restrições para tabelas `sign_in_response`
--
ALTER TABLE `sign_in_response`
  ADD CONSTRAINT `FK32lt55cv17x08xgtupifhfri1` FOREIGN KEY (`investor_id`) REFERENCES `investor` (`id`);

--
-- Restrições para tabelas `sign_in_response_perfis`
--
ALTER TABLE `sign_in_response_perfis`
  ADD CONSTRAINT `FKk6mop1reonrcr2ui8a3mldtlv` FOREIGN KEY (`sign_in_response_id`) REFERENCES `sign_in_response` (`id`),
  ADD CONSTRAINT `FKs2tp5mhxfxnl6iv84nx4ddxsq` FOREIGN KEY (`perfis_id`) REFERENCES `perfil` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
