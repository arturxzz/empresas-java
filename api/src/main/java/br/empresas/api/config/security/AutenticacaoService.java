package br.empresas.api.config.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.empresas.api.models.SignInResponse;
import br.empresas.api.repository.SignInRepository;


@Service
public class AutenticacaoService implements UserDetailsService {
	
	@Autowired
	private SignInRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<SignInResponse> sir = repository.findByInvestor_Email( username );
		
		if( sir.isPresent() ) {
			return sir.get();
		}
		
		throw new UsernameNotFoundException( "Login ou senha inválidos. Tente novamente." );
	}

}
