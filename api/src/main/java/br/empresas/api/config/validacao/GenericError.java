package br.empresas.api.config.validacao;

public class GenericError {
	
	String status;
	String error;
	
	public GenericError(String status, String error) {
		this.status = status;
		this.error = error;
	}
	
	public GenericError() {
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
