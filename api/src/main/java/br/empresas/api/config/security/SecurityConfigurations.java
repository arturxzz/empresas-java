package br.empresas.api.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.empresas.api.repository.SignInRepository;

@EnableWebSecurity
@Configuration
public class SecurityConfigurations extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AutenticacaoService autenticacaoService;

	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private SignInRepository sir;
	
	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	
	// AUTENTICACAO
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService( autenticacaoService ).passwordEncoder( new BCryptPasswordEncoder() );
	}
	
	// CONFIGURACAO DE AUTORIZACAO
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers( HttpMethod.POST, "/api/v1/users/auth/sign_in").permitAll()
//			.antMatchers( HttpMethod.GET, "/api/v1/enterprises").permitAll()
//			.antMatchers( HttpMethod.GET, "/api/v1/enterprises/*").permitAll()
			.anyRequest().authenticated()
			.and().csrf().disable()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and().addFilterBefore(	new AutenticacaoViaTokenFilter(tokenService, sir), 
									UsernamePasswordAuthenticationFilter.class );
	}
	
	// CONFIGURACAO DE RECURSOS ESTATICOS
	@Override
	public void configure(WebSecurity web) throws Exception {
	}
		
}
