package br.empresas.api.config.validacao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ValidationErrorHandler {
	
	@Autowired
	private MessageSource msgSrc;
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<FormError> handleInvalidArgument(MethodArgumentNotValidException exception) {
		List<FormError> dto = new ArrayList<>();
		List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
		
		fieldErrors.forEach(e -> {
			String msg = msgSrc.getMessage(e, LocaleContextHolder.getLocale());
			FormError erro = new FormError(e.getField(), msg );
			dto.add( erro );
		});
		
		return dto;
	}
	
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler( NumberFormatException.class )
	public GenericError handleNumberFormat(NumberFormatException exception) {		
		return new GenericError("404" , "Not Found");
	}
	
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler( GenericException.class )
	public GenericError handleGenericException(GenericException exception) {		
		return new GenericError("404" , "Not Found");
	}
}
