package br.empresas.api.config.validacao;

public class FormError {
	
	private String campo;
	private String erros;
	
	
	public FormError(String campo, String erros) {
		super();
		this.campo = campo;
		this.erros = erros;
	}
	
	public String getCampo() {
		return campo;
	}
	public String getErros() {
		return erros;
	}	

}
