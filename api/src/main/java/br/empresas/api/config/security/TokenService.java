package br.empresas.api.config.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.empresas.api.Utils;
import br.empresas.api.models.SignInResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {

	@Value("${api.jwt.expiration}")
	private String expiration;
	
	@Value("${api.jwt.secret}")
	private String secret;
	

	public String gerarToken(Authentication authentication) {
		SignInResponse sir = (SignInResponse) authentication.getPrincipal();
		Date hj = new Date();
		Date ex = new Date(hj.getTime() + Long.parseLong(expiration));
		return Jwts.builder()
				.setIssuer( "API Empresas" )
				.setSubject(sir.getId().toString())
				.setIssuedAt(hj)
				.setExpiration(ex)
				.signWith(SignatureAlgorithm.HS256, secret)
				.compact();
	}

	public boolean isTokenValido(String token) {
		try {
			Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
			return true;
		} catch(Exception e) {
			return false;
		}
	}

	public boolean isEmailClientValido(String email, String client) {
		return client.equals( Utils.encoder( email ) );
	}

	public Long getIdSignInResponse(String token) {
		Claims cl = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
		return Long.parseLong( cl.getSubject() );
	}
}
