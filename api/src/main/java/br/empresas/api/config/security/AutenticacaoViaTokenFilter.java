package br.empresas.api.config.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import br.empresas.api.models.SignInResponse;
import br.empresas.api.repository.SignInRepository;

public class AutenticacaoViaTokenFilter extends OncePerRequestFilter {
	
	private TokenService tokenService;
	
	private SignInRepository repository;
	
	public AutenticacaoViaTokenFilter(TokenService tokenService, SignInRepository repository) {
		this.tokenService = tokenService;
		this.repository = repository;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String token	= retornaToken( request );
		String email	= retornaEmail( request );
		String client	= retornaClient( request );
		
		boolean tokenValido;
		boolean emailClientValido;
		
		if( token != null && email != null && client != null) {
			emailClientValido	= tokenService.isEmailClientValido( email, client );			
			tokenValido			= tokenService.isTokenValido( token );
			
			if( emailClientValido && tokenValido ) {
				autenticarCliente(token);
			}
		} else {
			System.out.println( "Token  :" +token);
			System.out.println( "Email  :" +email);
			System.out.println( "Cliente:" +client);
			System.out.println( "----------------------------------------");			
		}

		filterChain.doFilter(request, response);
	}

	private void autenticarCliente(String token) {
		Long idSignInResponse = tokenService.getIdSignInResponse(token);
		SignInResponse sir = repository.findById( idSignInResponse ).get();
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(sir, null, sir.getAuthorities() );
		SecurityContextHolder.getContext().setAuthentication(authentication);		
	}

	private String retornaClient(HttpServletRequest request) {
		String client = request.getHeader("client");
		if( client == null || "".equals( client ) ) {
			return null;
		} else {
			return client;
		}
	}

	private String retornaEmail(HttpServletRequest request) {
		String email = request.getHeader("uid");
		if( email == null || "".equals( email ) ) {
			return null;
		} else {
			return email;
		}
	}

	private String retornaToken(HttpServletRequest request) {
		String token = request.getHeader("access-token");
		if( token == null || "".equals( token ) ) {
			return null;
		} else {
			return token;
		}
	}
}
