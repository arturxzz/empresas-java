package br.empresas.api.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.empresas.api.config.validacao.GenericException;
import br.empresas.api.controller.dto.EnterpriseDTO;
import br.empresas.api.models.Enterprise;
import br.empresas.api.repository.EnterpriseRepository;

@RestController
@RequestMapping(value="/api/{version}/enterprises/{index}")
public class EnterpriseDetalheController {
	
	@Autowired
	private EnterpriseRepository repository;
	
	@Autowired
	HttpServletRequest request;
	
	@GetMapping
	public ResponseEntity< EnterpriseDTO > retornaEnterprises( @PathVariable("index") String index ) throws GenericException {
		
		System.out.println( "Numero enviado: " + index );		
		Optional<Enterprise> res = repository.findById( Long.parseLong( index ) );
			
		if( res.isPresent() ) {
			return ResponseEntity.ok().body( new EnterpriseDTO( res.get() ) );
		} else {
			throw new GenericException();
		}
	}
}
