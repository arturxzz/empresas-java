package br.empresas.api.controller.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class SignInForm {
	
	@NotNull @NotEmpty
	private String email;
	@NotNull @NotEmpty
	private String password;
	
	public SignInForm(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	public SignInForm() {
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String senha) {
		this.password = senha;
	}

	public UsernamePasswordAuthenticationToken converter() {
		return new UsernamePasswordAuthenticationToken(email, password);
	}
}
