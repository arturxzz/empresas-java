package br.empresas.api.controller.dto;

import java.io.File;

import br.empresas.api.models.Enterprise;
import br.empresas.api.models.EnterpriseType;

public class EnterpriseForm {
	
	private Long id;
	private String email_enterprise;
	private String facebook;
	private String twitter;
	private String linkedin;
	private String phone;
	private boolean own_enterprise;
	private String enterprise_name;
	private String photo;
	private String description;
	private String city;
	private String country;
	private int value;
	private float sharePrice;
	private EnterpriseTypeForm enterprise_type;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail_enterprise() {
		return email_enterprise;
	}

	public void setEmail_enterprise(String email_enterprise) {
		this.email_enterprise = email_enterprise;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isOwn_enterprise() {
		return own_enterprise;
	}

	public void setOwn_enterprise(boolean own_enterprise) {
		this.own_enterprise = own_enterprise;
	}

	public String getEnterprise_name() {
		return enterprise_name;
	}

	public void setEnterprise_name(String enterprise_name) {
		this.enterprise_name = enterprise_name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public float getSharePrice() {
		return sharePrice;
	}

	public void setSharePrice(float sharePrice) {
		this.sharePrice = sharePrice;
	}

	public EnterpriseTypeForm getEnterprise_type() {
		return enterprise_type;
	}

	public void setEnterprise_type(EnterpriseTypeForm enterprise_type) {
		this.enterprise_type = enterprise_type;
	}

	public Enterprise converter( EnterpriseType et ) {
		
		return new Enterprise(	id, email_enterprise, facebook, twitter, linkedin, phone, 
								own_enterprise, enterprise_name, new File(photo), description, 
								city, country, value, sharePrice, et );
	}
}
