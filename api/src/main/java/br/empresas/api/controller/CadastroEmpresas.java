package br.empresas.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.empresas.api.controller.dto.EnterpriseForm;
import br.empresas.api.models.Enterprise;
import br.empresas.api.models.EnterpriseType;
import br.empresas.api.repository.EnterpriseRepository;
import br.empresas.api.repository.EnterpriseTypeRepository;

@RestController
@RequestMapping("/cadastro")
public class CadastroEmpresas {
	
	@Autowired
	private EnterpriseRepository enterpriseRepository;
	
	@Autowired
	private EnterpriseTypeRepository enterpriseTypeRepository;
	
	@PostMapping
	public ResponseEntity<EnterpriseForm> cadastrar(@RequestBody @Valid EnterpriseForm ef, 
													UriComponentsBuilder uriBuilder ) {
		
		System.out.println( ef.getEnterprise_type().getId() );
		System.out.println( ef.getEnterprise_type().getEnterprise_type_name() );
		
		EnterpriseType et = new EnterpriseType( ef.getEnterprise_type().getId(), 
												ef.getEnterprise_type().getEnterprise_type_name() );

		EnterpriseType enterpriseType = enterpriseTypeRepository.save( et );
		System.out.println( enterpriseType );
		
		Enterprise enterprise = ef.converter( enterpriseType );
		enterpriseRepository.save( enterprise );			
		
		return ResponseEntity.ok().body( ef );
	}
}
