package br.empresas.api.controller.dto;

import br.empresas.api.models.Portfolio;

public class PortfolioDTO {

	private int enterprises_number;
	private String[] enterprises;
	
	public PortfolioDTO(int enterprises_number, String[] enterprises) {
		super();
		this.enterprises_number = enterprises_number;
		this.enterprises = enterprises;
	}
	
	public PortfolioDTO(Portfolio portfolio) {
		this.enterprises_number = portfolio.getEnterprisesNumber();
		this.enterprises = portfolio.getEnterprisesAsArray();
	}
	
	public int getEnterprises_number() {
		return enterprises_number;
	}
	public void setEnterprises_number(int enterprises_number) {
		this.enterprises_number = enterprises_number;
	}
	public String[] getEnterprises() {
		return enterprises;
	}
	public void setEnterprises(String[] enterprises) {
		this.enterprises = enterprises;
	}
	
	
}
