package br.empresas.api.controller.dto;

import java.util.List;

public class ListEnterpriseForm {
	
	List<EnterpriseForm> lista;

	public List<EnterpriseForm> getLista() {
		return lista;
	}

	public void setLista(List<EnterpriseForm> lista) {
		this.lista = lista;
	}

	public ListEnterpriseForm(List<EnterpriseForm> lista) {
		this.lista = lista;
	}

	public ListEnterpriseForm() {
	}
	

}
