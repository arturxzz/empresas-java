package br.empresas.api.controller;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.empresas.api.Utils;
import br.empresas.api.config.security.TokenService;
import br.empresas.api.controller.dto.SignInForm;
import br.empresas.api.controller.dto.SignInResponseDTO;
import br.empresas.api.models.SignInResponse;
import br.empresas.api.repository.SignInRepository;

@RestController
@RequestMapping("/api/{version}/users/auth/sign_in")
public class AutenticacaoController {
	
	@Autowired
	AuthenticationManager authMan;
	
	@Autowired
	private TokenService tokenService;
		
	@Autowired
	private SignInRepository sirRepository;
		
	@PostMapping
	public ResponseEntity<SignInResponseDTO> signIn(@RequestBody @Valid SignInForm sif, 
													UriComponentsBuilder uriBuilder ) throws NoSuchAlgorithmException {
		
		UsernamePasswordAuthenticationToken loginData = sif.converter();
		
		try {
			Authentication authentication = authMan.authenticate( loginData );
			String token	= tokenService.gerarToken( authentication );
			String client	= Utils.encoder( sif.getEmail() ); 
			
			return ResponseEntity.ok()
					.header("access-token",	token)
					.header("token-type",	"Bearer")
					.header("uid",			sif.getEmail())
					.header("client",		client)
					.body( new SignInResponseDTO( retornaSIR( sif ) ) );
						
		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	private SignInResponse retornaSIR( SignInForm sif ) {
		Optional<SignInResponse> osir;

		osir = sirRepository.findByInvestor_Email( sif.getEmail() );
		
		if( osir.isPresent() ) {
			return osir.get();
		} else {
			return null;
		}
	}
}
