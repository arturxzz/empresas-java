package br.empresas.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import br.empresas.api.controller.dto.EnterpriseDTO;
import br.empresas.api.models.Enterprise;
import br.empresas.api.repository.EnterpriseRepository;

@RestController
@RequestMapping(value="/api/{version}/enterprises")
public class EnterprisesFilterController {
	
	@Autowired
	private EnterpriseRepository repository;
	
	@GetMapping
	public ResponseEntity< List<EnterpriseDTO> > retornaEnterprises(String enterprise_types, String name) {
		
		boolean temNome = !( name == null || "".equals( name ) );
		boolean temType = !( enterprise_types == null || "".equals( enterprise_types ) );
		
		List<EnterpriseDTO> retorno = new ArrayList<EnterpriseDTO>();
		
		if( temNome && temType ) {
			Optional<List<Enterprise>> enterprises = repository.findByEnterpriseNameAndEnterpriseType_id(name, Long.parseLong(enterprise_types));
			if( enterprises.isPresent() ) {
				retorno =  new EnterpriseDTO().converter( enterprises.get() );
			}
		} else if( temNome ) {
			Optional<List<Enterprise>> enterprises = repository.findByEnterpriseName( name );
			if( enterprises.isPresent() ) {
				retorno =  new EnterpriseDTO().converter( enterprises.get() );
			}
		} else if( temType ) {
			Optional<List<Enterprise>> enterprises = repository.findByEnterpriseType_id( Long.parseLong(enterprise_types) );
			if( enterprises.isPresent() ) {
				retorno =  new EnterpriseDTO().converter( enterprises.get() );
			}
		} else {
			retorno = new EnterpriseDTO().converter( repository.findAll() );
		}
		return ResponseEntity.ok().body( retorno );
	}

}
