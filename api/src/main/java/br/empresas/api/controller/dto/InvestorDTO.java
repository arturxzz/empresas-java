package br.empresas.api.controller.dto;

import br.empresas.api.models.Investor;

public class InvestorDTO {
	
	private Long id;
	private String investor_name;
	private String email;
	private String city;
	private String country;
	private float balance;
	private byte[] photo;
	private PortfolioDTO portfolio;
	private float portfolio_value;
	private boolean first_access;
	private boolean super_angel;
	
	public InvestorDTO(Investor i) {
		this.id					= i.getId();
		this.investor_name		= i.getInvestorName();
		this.email				= i.getEmail();
		this.city				= i.getCity();
		this.country			= i.getCountry();
		this.balance			= i.getBalance();
		this.photo				= i.getPhoto();
		this.portfolio			= new PortfolioDTO( i.getPortfolio() );
		this.portfolio_value	= i.getPortfolioValue();
		this.first_access		= i.isFirstAccess();
		this.super_angel		= i.isSuperAngel();
	}
	public InvestorDTO() {
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInvestor_name() {
		return investor_name;
	}
	public void setInvestor_name(String investor_name) {
		this.investor_name = investor_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public byte[] getPhoto() {
		return photo;
	}
	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}
	public PortfolioDTO getPortfolio() {
		return portfolio;
	}
	public void setPortfolio(PortfolioDTO portfolio) {
		this.portfolio = portfolio;
	}
	public float getPortfolio_value() {
		return portfolio_value;
	}
	public void setPortfolio_value(float portfolio_value) {
		this.portfolio_value = portfolio_value;
	}
	public boolean isFirst_access() {
		return first_access;
	}
	public void setFirst_access(boolean first_access) {
		this.first_access = first_access;
	}
	public boolean isSuper_angel() {
		return super_angel;
	}
	public void setSuper_angel(boolean super_angel) {
		this.super_angel = super_angel;
	}
}
