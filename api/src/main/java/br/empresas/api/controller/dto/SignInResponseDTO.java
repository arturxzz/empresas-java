package br.empresas.api.controller.dto;

import java.util.List;
import java.util.stream.Collectors;

import br.empresas.api.models.SignInResponse;

public class SignInResponseDTO {
	
	private InvestorDTO investor;
	private String enterprise;
	private boolean success;
	
	public SignInResponseDTO(SignInResponse sir) {
		this.investor			= new InvestorDTO( sir.getInvestor() );
		this.enterprise			= sir.isEnterprise();
		this.success			= sir.isSuccess();
	}
	
	public InvestorDTO getInvestor() {
		return investor;
	}

	public void setInvestor(InvestorDTO investor) {
		this.investor = investor;
	}

	public String getEnterprise() {
		return enterprise;
	}
	public void setEnterprise(String enterprise) {
		this.enterprise = enterprise;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}

	public static List<SignInResponseDTO> converter(List<SignInResponse> lSir) {
		return lSir.stream().map(SignInResponseDTO::new).collect( Collectors.toList() );
	}
	
	public static SignInResponseDTO converter(SignInResponse sir) {
		return new SignInResponseDTO( sir );
	}
}
