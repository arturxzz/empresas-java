package br.empresas.api.controller.dto;

public class EnterpriseTypeDTO {
	
	private Long id;
	private String enterprise_type_name;
	
	public EnterpriseTypeDTO(Long id, String enterprise_type_name) {
		this.id = id;
		this.enterprise_type_name = enterprise_type_name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEnterprise_type_name() {
		return enterprise_type_name;
	}
	public void setEnterprise_type_name(String enterprise_type_name) {
		this.enterprise_type_name = enterprise_type_name;
	}
	
}
