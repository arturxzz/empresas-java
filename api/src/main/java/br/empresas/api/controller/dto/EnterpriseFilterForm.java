package br.empresas.api.controller.dto;

public class EnterpriseFilterForm {
	
	private String enterprise_types;
	private String name;
	
	public EnterpriseFilterForm(String enterprise_types, String name) {
		this.enterprise_types = enterprise_types;
		this.name = name;
	}
	
	public EnterpriseFilterForm() {
	}

	public String getEnterprise_types() {
		return enterprise_types;
	}
	public void setEnterprise_types(String enterprise_types) {
		this.enterprise_types = enterprise_types;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
