package br.empresas.api;

import java.util.Base64;

public class Utils {
	
	// CRIPTOGRAFANDO STRING
	public static String encoder(String str) {
	    return new String(Base64.getEncoder().encode(str.getBytes()));
	}
	
	// DESCRIPTOGRAFANDO STRING
	public static String decoder(String str) {
	    return new String(Base64.getDecoder().decode(str.getBytes()));
	}
}
