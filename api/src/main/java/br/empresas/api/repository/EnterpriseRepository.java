package br.empresas.api.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.empresas.api.models.Enterprise;

public interface EnterpriseRepository extends JpaRepository<Enterprise, Long> {
	
	Optional<List<Enterprise>> findByEnterpriseName(String enterpriseName);
	Optional<List<Enterprise>> findByEnterpriseType_id(Long id);
	Optional<List<Enterprise>> findByEnterpriseNameAndEnterpriseType_id(String enterpriseName, Long id);
	
}
