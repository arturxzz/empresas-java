package br.empresas.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.empresas.api.models.SignInResponse;

public interface SignInRepository extends JpaRepository<SignInResponse, Long> {

	Optional<SignInResponse> findByInvestor_Email(String email);
	Optional<SignInResponse> findByInvestor_EmailAndInvestor_Password(String email, String password);
}
