package br.empresas.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.empresas.api.models.EnterpriseType;

public interface EnterpriseTypeRepository extends JpaRepository<EnterpriseType, Long> {

}
