package br.empresas.api.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class EnterpriseType {
	
	@Id @GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;
	private String enterpriseTypeName;
	
	public EnterpriseType() {
	}	
	
	public EnterpriseType(Long id, String enterpriseTypeName) {
		this.id = id;
		this.enterpriseTypeName = enterpriseTypeName;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEnterpriseTypeName() {
		return enterpriseTypeName;
	}
	public void setEnterpriseTypeName(String enterpriseTypeName) {
		this.enterpriseTypeName = enterpriseTypeName;
	}
	
	

}
