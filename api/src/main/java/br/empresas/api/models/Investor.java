package br.empresas.api.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Investor {
	
	@Id @GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;
	private String password;
	private String investorName;
	private String email;
	private String city;
	private String country;
	private float balance;
	private byte[] photo;
	
	@OneToOne
	private Portfolio portfolio;
	private float portfolioValue;
	private boolean firstAccess;
	private boolean superAngel;

	public Investor(Long i, String investorName, String email, String city, String country, float d, byte[] photo,
			Portfolio portfolio, float e, boolean firstAccess, boolean superAngel) {
		this.id = i;
		this.investorName = investorName;
		this.email = email;
		this.city = city;
		this.country = country;
		this.balance = d;
		this.photo = photo;
		this.portfolio = portfolio;
		this.portfolioValue = e;
		this.firstAccess = firstAccess;
		this.superAngel = superAngel;
	}

	public Investor() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String passwd) {
		this.password = passwd;
	}

	public String getInvestorName() {
		return investorName;
	}

	public void setInvestorName(String investorName) {
		this.investorName = investorName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public Portfolio getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(Portfolio portfolio) {
		this.portfolio = portfolio;
	}

	public float getPortfolioValue() {
		return portfolioValue;
	}

	public void setPortfolioValue(float portfolioValue) {
		this.portfolioValue = portfolioValue;
	}

	public boolean isFirstAccess() {
		return firstAccess;
	}

	public void setFirstAccess(boolean firstAccess) {
		this.firstAccess = firstAccess;
	}

	public boolean isSuperAngel() {
		return superAngel;
	}

	public void setSuperAngel(boolean superAngel) {
		this.superAngel = superAngel;
	}
}