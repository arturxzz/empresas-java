package br.empresas.api.models;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Portfolio {

	@Id @GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;

	private int enterprisesNumber;
	private ArrayList<String> enterprises;

	public Portfolio(int enterprisesNumber, ArrayList<String> enterprises) {
		this.enterprisesNumber = enterprisesNumber;
		this.enterprises = enterprises;
	}

	public Portfolio() {
	}

	public Portfolio(int enterprisesNumber) {
		this.enterprisesNumber = enterprisesNumber;
		this.enterprises = new ArrayList<String>();
	}

	public int getEnterprisesNumber() {
		return enterprisesNumber;
	}

	public void setEnterprisesNumber(int enterprisesNumber) {
		this.enterprisesNumber = enterprisesNumber;
	}

	public ArrayList<String> getEnterprises() {
		return enterprises;
	}

	public String[] getEnterprisesAsArray() {
		if( this.enterprises == null ) {
			return new String[0];
		} else {
			String arr[] = new String[ enterprises.size() ];
			for( int i = 0; i < enterprises.size(); i++ ) {
				arr[i] = enterprises.get( i );
			}
			return arr;
		}
	}

	public void setEnterprises(ArrayList<String> enterprises) {
		this.enterprises = enterprises;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}