package br.empresas.api.models;

import java.io.File;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Enterprise {
	
	@Id @GeneratedValue( strategy = GenerationType.IDENTITY )
	private Long id;
	private String emailEnterprise;
	private String facebook;
	private String twitter;
	private String linkedin;
	private String phone;
	private boolean ownEnterprise;
	private String enterpriseName;
	private File photo;
	private String description;
	private String city;
	private String country;
	private int value;
	private float sharePrice;
	
	@OneToOne
	private EnterpriseType enterpriseType;
	
	public Enterprise() {
	}

	public Enterprise(Long id, String emailEnterprise, String facebook, String twitter, String linkedin, String phone,
			boolean ownEnterprise, String enterpriseName, File photo, String description, String city, String country,
			int value, float sharePrice, EnterpriseType enterpriseType) {
		this.id = id;
		this.emailEnterprise = emailEnterprise;
		this.facebook = facebook;
		this.twitter = twitter;
		this.linkedin = linkedin;
		this.phone = phone;
		this.ownEnterprise = ownEnterprise;
		this.enterpriseName = enterpriseName;
		this.photo = photo;
		this.description = description;
		this.city = city;
		this.country = country;
		this.value = value;
		this.sharePrice = sharePrice;
		this.enterpriseType = enterpriseType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmailEnterprise() {
		return emailEnterprise;
	}

	public void setEmailEnterprise(String emailEnterprise) {
		this.emailEnterprise = emailEnterprise;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isOwnEnterprise() {
		return ownEnterprise;
	}

	public void setOwnEnterprise(boolean ownEnterprise) {
		this.ownEnterprise = ownEnterprise;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public File getPhoto() {
		return photo;
	}

	public void setPhoto(File photo) {
		this.photo = photo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public float getSharePrice() {
		return sharePrice;
	}

	public void setSharePrice(float sharePrice) {
		this.sharePrice = sharePrice;
	}

	public EnterpriseType getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(EnterpriseType enterpriseType) {
		this.enterpriseType = enterpriseType;
	}
	
}
